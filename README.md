# My Mirror Technical Task

## Setup

Make sure to install the dependencies:

```bash
npm install
```

## Other Requirements

Node 16 LTS

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```
