import dayjs from 'dayjs';
// import localizedFormat from 'dayjs/plugin/localizedFormat';
import updateLocale from 'dayjs/plugin/updateLocale';
import advancedFormat from 'dayjs/plugin/advancedFormat';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';

// dayjs.extend(localizedFormat);
dayjs.extend(utc);
dayjs.extend(advancedFormat);
dayjs.extend(timezone);
dayjs.extend(updateLocale);
dayjs.tz.setDefault("Australia/Sydney");

export default defineNuxtPlugin(() => {
  return {
    provide: {
      dayjs,
    },
  };
});